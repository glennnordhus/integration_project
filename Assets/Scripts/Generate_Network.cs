﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Generate_Network : MonoBehaviour
{
    public GameObject Node_Prefab;
    public GameObject Connection_Prefab;

    public Valve.VR.InteractionSystem.Sample.BuggyBuddy buggy;

    private Neural_Network_Data networkData;

    private GameObjectStorage gameObjectStorage;
    private List<ulong> ids;

    // Start is called before the first frame update
    void Start()
    {
        networkData = new Neural_Network_Data(this);
        gameObjectStorage = new GameObjectStorage();
        ids = new List<ulong>();
    }


    float timer = 2.0f;
    // Update is called once per frame
    void Update()
    {
        //timer -= Time.deltaTime;
        //if(timer < 0.0f)
        {
            //timer = 0.1f;
            //
            //double[] input = networkData.debug_fillFirstLayerWithRandomData();
            //
            ////Debug.Log("Input = " + string.Join("  ",
            //// new List<double>(input)
            //// .ConvertAll(i => i.ToString())
            //// .ToArray()));
            //
            //networkData.ActivateNetwork();
            //
            ////Debug.Log("Activated Network");
            //
            //double[] output = networkData.getOutputNodeValues();
            //
            ////Debug.Log("Output = " + string.Join("  ",
            //// new List<double>(output)
            //// .ConvertAll(i => i.ToString())
            //// .ToArray()));
            ////
            ////Debug.Log("The Sigmoid of 0.0 is " + Neural_Network_Data.Sigmoid(0.0));
            //
            //
            //buggy.steer = new Vector2((float)output[0], (float)output[1]);
            //buggy.throttle = (float)output[2];
            //buggy.handBrake = 0.0f;
            //
            //GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //cube.transform.position = new Vector3(Time.time * 8.0f, 0.0f, 0.0f);
            //
            //ids.Add(gameObjectStorage.Add(cube));
            //
            //if(Time.time > 5.0f && ids.Count > 2)
            //{
            //    gameObjectStorage.Delete(ids[0]);
            //    ids.RemoveAt(0);
            //    gameObjectStorage.Delete(ids[0]);
            //    ids.RemoveAt(0);
            //}
        }
    }

    public double[] activateNetwork(double[] data)
    {
        networkData.fillFirstLayerData(data);

        networkData.ActivateNetwork();

        return networkData.getOutputNodeValues();
    }

    public void UpdateNodePosition(ulong nodeID, int layer)
    {
        networkData.UpdateNodePos(nodeID, layer);
    }

    public void updateConnnectionWeight(ulong connectionID, float weight)
    {
        networkData.UpdateConnectionWeight(connectionID, weight);
    }
}
