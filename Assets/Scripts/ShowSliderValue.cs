﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowSliderValue : MonoBehaviour
{
    public Slider sliderUI;
    private Text textDisplay;

    public SpawnConnectorUI spawnConnector;
    // Start is called before the first frame update
    void Start()
    {
        textDisplay = GetComponent<Text>();
        sliderUI.value = spawnConnector.weight;
    }

    public void updateText()
    {
        textDisplay.text = sliderUI.value.ToString();
        spawnConnector.weight = sliderUI.value;
    }
}
