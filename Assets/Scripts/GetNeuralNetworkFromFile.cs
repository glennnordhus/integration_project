﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public class GetNeuralNetworkFromFile
{

    public struct Layer
    {
        public string type;
        public Vector2Int inputShape;
        public int numberOfNodes;
        public string activition;
        public List<List<double>> weights;
    }
    private string ModelName;

    private string inputShape;
    private string Model;
    private string ModelData;

    private List<Layer> layers;

    private int depth = 1;

    public GetNeuralNetworkFromFile()
    {
        
        layers = new List<Layer>();
    }
    /// <summary>
    /// Collectes the model and the model data from the filename. The filename has to be the same for both files.
    /// </summary>
    /// <param name="filename"></param>
    /// <returns></returns>
    public  List<Layer> Getfile(String filename)
    {
        string returnString = filename;
        //a JSON file of the model
        Model = System.IO.File.ReadAllText("Assets\\" + filename + ".json");
        //a HDF5 file of the weights
        ModelData = System.IO.File.ReadAllText("Assets\\" + filename + ".txt");

        string[] Lines = Model.Split('\n'); // seperate in to lines.
        int LinesNumber = Lines.Length;     
        Lines = Lines.Select(x => x.Replace("\t", string.Empty)).ToArray(); // removes tabs
        Lines = Lines.Select(x => x.Replace(" ", string.Empty)).ToArray();  // remove white spaces
        for (int i = 0; i < LinesNumber; i++)
        {
            string temp = Lines[i];
            if (temp.Contains("config"))
            {
                ModelName = FindJsonValue(Lines[++i]);               
                //gets to the layers 
                while (temp.Contains("layers") == false)
                {                    
                    temp = Lines[++i];
                   
                }

                string type = "";
                
                int numberOfNodes = 0;
                string activition = "";
                // while inside the json array will break if there are multiple arrays over several lines. 
                while (!(Lines[++i].Contains("]") && !Lines[i].Contains("["))) 
                {
                    //fines only the first name which is the network type
                    if(depth == 1 && Lines[i].Contains("class_name"))
                    {                     
                        type = FindJsonValue(Lines[i]);
                    }
                    if (Lines[i].Contains("batch_input_shape"))
                    {
                        inputShape = FindJsonValue(Lines[i]);
                    }
                    if (Lines[i].Contains("units"))
                    {
                        Debug.Log(FindJsonValue(Lines[i]));
                        numberOfNodes = Int32.Parse(FindJsonValue(Lines[i]));
                    }
                    if (Lines[i].Contains("activation"))
                    {
                        activition = FindJsonValue(Lines[i]);
                    }


                    if (EndLayer(Lines[i]))
                    {
                        //checks the network type. this can save different things dependent on the type
                        if (type.CompareTo("Dense") == 0)
                        {
                            Layer newLayer;
                            newLayer.weights = new List<List<double>>();
                            newLayer.type = type;
                            newLayer.inputShape = new Vector2Int();
                            newLayer.numberOfNodes = numberOfNodes;
                            newLayer.activition = activition;
                            type = "";
                            
                            numberOfNodes = 0;
                            activition = "";
                            depth = 1;
                            layers.Add(newLayer);
                        }
                        

                    }
                }
            }
        }
        ModelData = ModelData.Replace("\n", String.Empty);
        string[] tempData = ModelData.Split(']');
        tempData = tempData.Select(x => x += "]").ToArray();
        int layernumber = 0;
        for(int i = 0; i < tempData.Length; i++)
        {
            if(tempData[i].Contains("[["))
            {
                
                int g = 0;
                while (!tempData[i].Equals("]"))
                {
                    //Debug.Log(tempData[i]);
                   
                    layers[layernumber].weights.Add(new List<double>());

                    string tempString = tempData[i];
                    tempString = tempString.Replace("[", String.Empty);
                    tempString = tempString.Replace("]", String.Empty);
                    RegexOptions options = RegexOptions.None;
                    Regex regex = new Regex("[ ]{2,}", options);
                    tempString = regex.Replace(tempString, " ");
                    //Debug.Log(tempString);
                    string[] NumberArray = tempString.Split(' ');


                    //Debug.Log(NumberArray.Length);
                    for (int f = 0; f < NumberArray.Length; f++)
                    {
                        //Debug.Log(NumberArray[f] + " : " + f);
                        if (NumberArray[f].Length > 0 )
                        {
                            layers[layernumber].weights[g].Add(double.Parse(NumberArray[f], CultureInfo.InvariantCulture));
                        }
                        
                    }
                    g++;
                    i++;
                }

                layernumber++;
            }
            
        }

        inputShape = FindJsonValue(inputShape);

        inputShape = inputShape.Replace("[", "");
        inputShape = inputShape.Replace("]", "");
        string[] split = inputShape.Split(',');
        int xShape, yShape;
        if(split[0].Contains("null"))
        {
            yShape = 1;
        }
        else
        {
            yShape = Int32.Parse(split[0]);
        }
        if (split[1].Contains("null"))
        {
            xShape = 1;
        }
        else
        {
            xShape = Int32.Parse(split[1]);
        }
        Debug.Log("x = " + xShape + "   y = " + yShape);
       
        Layer copy = layers[0];
        copy.inputShape.Set(xShape, yShape);
        layers[0] = copy;
        Debug.Log(layers[0].inputShape);
        
        /*
        foreach(Layer lay in layers)
        {
            
            foreach (List<double> dob in lay.weights)
            {
                string temp = "";
                foreach(double d in dob)
                {
                    temp += d.ToString() + " ";
                }
                Debug.Log(temp);
            }
            
            Debug.Log(lay.inputShape);
        }
        */




        return layers;
    }

    bool EndLayer(string line)
    {
        bool endLayer = false;
        if (line.Contains("}"))
        {
            depth--;
        }
        if(depth < 1)
        {
            endLayer =  true;
        }
        if (line.Contains("{"))
        {
            depth++;
        }
        return endLayer;
    }


    string FindJsonValue(string jsonLine)
    {
        string value;
        if (jsonLine.Contains(':'))
        {
            value = jsonLine.Split(':')[1];
            value = value.Remove(value.Length - 1);
            value = value.Replace("\"", "");
            return value;
        }
        return jsonLine;
    }




}

