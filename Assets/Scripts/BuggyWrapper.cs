﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuggyWrapper : MonoBehaviour
{
    public Vector2 steer;
    public float throttle;
    public float handbrake;

    public Valve.VR.InteractionSystem.Sample.BuggyBuddy buggy;

    public Generate_Network network;

    public RenderTexture renderTexture;

    // Start is called before the first frame update
    void Start()
    {
        steer = new Vector2();

        
        StartCoroutine(buggyAI());
    }

    // Update is called once per frame
    void Update()
    {
        buggy.steer = steer;
        buggy.throttle = throttle;
        buggy.handBrake = handbrake;
    }

    public void recieveData(double[] data)
    {
        Debug.Assert(data.Length == 3, "The data recieved for the buggy has the wrong length");

        steer.x = (float)data[0];
        throttle = (float)data[1];
        handbrake = (float)data[2];
    }

    public IEnumerator buggyAI()
    {
        yield return new WaitForEndOfFrame();
        while(true)
        {
            Texture2D texture2D = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.RGB24, false);

            RenderTexture.active = renderTexture;
            texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
            texture2D.Apply();

            Color sample = texture2D.GetPixel(1, 1);

            /*double[] input = { Mathf.Round(sample.r), Mathf.Round(sample.g), Mathf.Round(sample.b)};
            

            Debug.Log("Input 0: " + input[0]);
            Debug.Log("Input 1: " + input[1]);
            Debug.Log("Input 2: " + input[2]);

            double[] output = network.activateNetwork(input);

            Debug.Log("Output 0: " + output[0]);
            Debug.Log("Output 1: " + output[1]);
            Debug.Log("Output 2: " + output[2]);
            

            recieveData(output);
            */
            yield return new WaitForSeconds(0.2f);
        }
    }
}
