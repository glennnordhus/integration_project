﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendValueChangedToConnection : MonoBehaviour
{
    private Generate_Network networkGenerator;
    public ulong connectionID;

    // Start is called before the first frame update
    void Start()
    {
        networkGenerator = GameObject.FindObjectOfType<Generate_Network>();
    }
    

    public void sendValueChanged(float value)
    {
        networkGenerator.updateConnnectionWeight(connectionID, value);
    }
}
