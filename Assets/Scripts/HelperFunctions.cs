﻿using UnityEngine;
using System.Collections;

namespace HelperFunctions
{
    public static class VectorMath
    {
        public static Vector3 ClosestPointOnLine(Vector3 vA, Vector3 vB, Vector3 vPoint)
        {
            Vector3 vVector1 = vPoint - vA;
            Vector3 vVector2 = (vB - vA).normalized;

            float d = Vector3.Distance(vA, vB);
            float t = Vector3.Dot(vVector2, vVector1);

            if (t <= 0)
                return vA;

            if (t >= d)
                return vB;

            Vector3 vVector3 = vVector2 * t;

            Vector3 vClosestPoint = vA + vVector3;

            return vClosestPoint;
        }
    }
}
