﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
using Valve.VR;


[RequireComponent( typeof(Interactable))]
public class NodeInteraction : MonoBehaviour
{
    private bool isHovering;
    private bool attached;

    private readonly SteamVR_Input_Sources handType;
    public SteamVR_Action_Boolean grabAction;

    private Hand hoveringHand;

    [HideInInspector]
    public ulong nodeID;
    [HideInInspector]
    public int layer;

    float cooldown;

    Generate_Network networkGenerator;

    // Start is called before the first frame update
    void Start()
    {
        cooldown = 0.0f;
        isHovering = false;
        networkGenerator = GameObject.Find("NetworkGenerator").GetComponent<Generate_Network>();
    }

    // Update is called once per frame
    void Update()
    {
        if(cooldown > 0.0f)
        {
            cooldown -= Time.deltaTime;
            return;
        }

        if(grabAction.GetState(handType))
        {
            if(isHovering && !attached)
            {
                hoveringHand.AttachObject(gameObject, GrabTypes.Pinch);
                attached = true;
                cooldown = 0.2f;
                StartCoroutine("UpdateConnectionTransforms");
            }
            else if(attached)
            {
                hoveringHand.DetachObject(gameObject);
                attached = false;
                cooldown = 0.2f;
                StopCoroutine("UpdateConnectionTransforms");
            }
        }
    }

    IEnumerator UpdateConnectionTransforms()
    {
        while(true)
        {
            networkGenerator.UpdateNodePosition(nodeID, layer);
            yield return null;
        }
    }


    private void OnHandHoverBegin(Hand hand)
    {
        isHovering = true;

        hoveringHand = hand;
    }

    private void OnHandHoverEnd(Hand hand)
    {
        isHovering = false;
    }
}
