﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Valve.VR.InteractionSystem;

public class SpawnConnectorUI : MonoBehaviour
{
    public GameObject connectionUI;


    private GameObject playerHead;

    private GameObject spawnedUI;

    [HideInInspector]
    public ulong connectorID;
    [HideInInspector]
    public bool UIExist;
    [HideInInspector]
    public float weight;

    // Start is called before the first frame update
    void Start()
    {
        UIExist = false;

        playerHead = Player.instance.headCollider.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnUI()
    {
        if(spawnedUI != null)
        {
            return;
        }
        UIExist = true;

        float endOffset = transform.localScale.y;
        Vector3 endPosition1 = transform.position + transform.up * endOffset;
        Vector3 endPosition2 = transform.position - transform.up * endOffset;

        Vector3 spawnPosition = HelperFunctions.VectorMath.ClosestPointOnLine(endPosition1, endPosition2, playerHead.transform.position) + new Vector3(0.0f, 0.5f, 0.0f);

        spawnedUI = Instantiate(connectionUI, spawnPosition, Quaternion.LookRotation(spawnPosition - playerHead.transform.position));

        spawnedUI.GetComponentInChildren<ShowSliderValue>().spawnConnector = this;
        spawnedUI.GetComponentInChildren<SendValueChangedToConnection>().connectionID = connectorID;
    }
}
