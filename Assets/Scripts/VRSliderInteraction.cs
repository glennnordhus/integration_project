﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class VRSliderInteraction : MonoBehaviour
{
    public GameObject sliderObject;

    private Slider slider;
    private Hand attachedHand;
    private bool attached;
    Vector3 maxPos;
    Vector3 minPos;
    float maxDist;

    public SteamVR_Action_Boolean grabAction;

    private float secondsToDespawn;
    private const float DEFAULT_DESPAWN_TIME = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
        slider = sliderObject.GetComponent<Slider>();
        //Debug.Log(transform.position);
        //Debug.Log(transform.Find("Handle Slide Area/Handle").transform.position);
        attached = false;

        slider.value = slider.maxValue;
        maxPos = transform.position;
        minPos = sliderObject.transform.position - (maxPos - sliderObject.transform.position);
        maxDist = (maxPos - minPos).magnitude;

        secondsToDespawn = DEFAULT_DESPAWN_TIME;
    }

    // Update is called once per frame
    void Update()
    {
        secondsToDespawn -= Time.deltaTime;

        if (secondsToDespawn < 0.0f)
        {
            Destroy(slider.transform.parent.gameObject);
        }

        if (attached)
        {
            Vector3 closestPoint = HelperFunctions.VectorMath.ClosestPointOnLine(minPos, maxPos, attachedHand.transform.position);

            float currDistance = (maxPos - closestPoint).magnitude;

            slider.normalizedValue = 1.0f - (currDistance / maxDist);

            if(!grabAction.GetState(attachedHand.handType))
            {
                attached = false;
            }

            secondsToDespawn = DEFAULT_DESPAWN_TIME;
        }
    }


    public void AttachToHand(Hand hand)
    {
        attachedHand = hand;
        attached = true;
    }
}
