﻿using System.Collections.Generic;
using UnityEngine;


public class Neural_Network_Data
{
    Generate_Network parent;

    GenericObjectStorage<Connection> connections;

    List<Neural_Network_Layer> layers;

    private GetNeuralNetworkFromFile NetworkFromFile;

    List<GetNeuralNetworkFromFile.Layer> file;

    public Neural_Network_Data(Generate_Network parent)
    {
        this.parent = parent;

        connections = new GenericObjectStorage<Connection>();
        layers = new List<Neural_Network_Layer>();
        NetworkFromFile = new GetNeuralNetworkFromFile();

       

        //CreateDense(3, new Vector3(0.0f, .5f, 0.0f), 3);
        //CreateDense(3, new Vector3(0.0f, .5f, 10.0f), 4);


        //string[] input = { "Red", "Green", "Blue" };

        //layers[0].nameNodes(input);

        string[] output = {"Steer", "Throttle", "HandBreak"};

        //layers[1].nameNodes(output);

        CreateFromFile();
    }

    public void CreateFromFile()
    {
        file = NetworkFromFile.Getfile("model");
        layers.Add(new Neural_Network_Layer(Neural_Network_Layer.Layer_Type.DENSE, (uint)file[0].inputShape.x * (uint)file[0].inputShape.y, new Vector3(0.0f, .5f, 0.0f), parent, 0, (uint)file[0].inputShape.x));
        int i = 1;
        foreach(GetNeuralNetworkFromFile.Layer lay in file)
        {
            layers.Add(new Neural_Network_Layer(Neural_Network_Layer.Layer_Type.DENSE, (uint)lay.numberOfNodes, new Vector3(0.0f, .5f, 10.0f * i), parent, i++, (uint)lay.numberOfNodes));
            
            for(int x = 0; x < layers[i - 1].nodes.Length; ++x) //Node node1 in layers[layers.Count - 2].nodes.GetCollection())
            {
                for (int y = 0; y < layers[i - 2].nodes.Length; ++y) //Node node2 in layers[layers.Count - 2].nodes.GetCollection())
                {
                    try
                    {
                        
                        ulong id = GenerateNewConnection(layers[i - 1].nodes.GetCollection()[x], layers[i - 2].nodes.GetCollection()[y], lay.weights[y][x]);

                        layers[i - 1].nodes.GetCollection()[x].outgoingConnections.Add(id);
                        layers[i - 2].nodes.GetCollection()[y].incomingConnections.Add(id);
                    }
                    catch (System.NullReferenceException e)
                    {
                        Debug.Log(e.Message);

                        break;
                    }
                    
                }
            }
        }


    }

    public void CreateDense(uint number_of_nodes, Vector3 offset, uint nodes_per_row = 10)
    {
        layers.Add(new Neural_Network_Layer(Neural_Network_Layer.Layer_Type.DENSE, number_of_nodes, offset, parent, layers.Count, nodes_per_row));

        if(layers.Count > 1)
        {
            foreach(Node node1 in layers[layers.Count - 2].nodes.GetCollection())
            {
                foreach (Node node2 in layers[layers.Count - 1].nodes.GetCollection())
                {
                    try
                    {
                        ulong id = GenerateNewConnection(node1, node2);

                        node1.outgoingConnections.Add(id);
                        node2.incomingConnections.Add(id);
                    }
                    catch(System.NullReferenceException e)
                    {
                        Debug.Log(e.Message);

                        break;
                    }

                }
            }
        }
    }

    public ulong GenerateNewConnection(Node incomingNode, Node outgoingNode, double weight = 0.0f)
    {
        Connection newConnection = new Connection();
    
        ulong newID = connections.GetNextID();
        newConnection.id = newID;
    
        Transform incNode = incomingNode.model.transform;
        Transform outNode = outgoingNode.model.transform;
        
        newConnection.model = GameObject.Instantiate(parent.Connection_Prefab, parent.transform);
        newConnection.model.GetComponent<SpawnConnectorUI>().connectorID = newID;

        UpdateConnectionTransform(newConnection.model, incNode, outNode);

        newConnection.incoming = incomingNode.id;
        newConnection.outgoing = outgoingNode.id;
    
        newConnection.weight = weight;

        newConnection.model.GetComponent<SpawnConnectorUI>().weight = (float)newConnection.weight;

        ColorCodeConnection(newConnection.model, newConnection.weight);

        connections.Add(newConnection);
    
        return newID;
    }

    public void UpdateNodePos(ulong nodeID, int layer)
    {
        Transform updatedNode = layers[layer].nodes.Get(nodeID).model.transform;

        foreach(ulong connectionID in layers[layer].nodes.Get(nodeID).incomingConnections)
        {
            Transform inNode = layers[layer - 1].nodes.Get(connections.Get(connectionID).incoming).model.transform;

            UpdateConnectionTransform(connections.Get(connectionID).model, inNode, updatedNode);
        }

        foreach(ulong connectionID in layers[layer].nodes.Get(nodeID).outgoingConnections)
        {
            Transform outNode = layers[layer + 1].nodes.Get(connections.Get(connectionID).outgoing).model.transform;

            UpdateConnectionTransform(connections.Get(connectionID).model, updatedNode, outNode);
        }
    }

    public void ActivateNetwork()
    {
        for(int i = 0; i < layers.Count; ++i)
        {
            for(int j = 0; j < layers[i].nodes.Length; ++j)
            {
                try
                {
                    double value = 0.0;
                    
                    if (i == 0)
                    {
                        value = layers[i].nodes.GetCollection()[j].value;
                    }
                    else
                    {
                        foreach (ulong connectionID in layers[i].nodes.GetCollection()[j].incomingConnections)
                        {
                            value += connections.Get(connectionID).value * connections.Get(connectionID).weight;
                        }

                        layers[i].nodes.GetCollection()[j].value = value;
                    }

                   
                    switch(layers[i].function)
                    {
                        case Neural_Network_Layer.Activation_Function.LINEAR:
                            //Linear does nothing
                            break;

                        case Neural_Network_Layer.Activation_Function.SIGMOID:
                            value = Sigmoid(value);
                            break;
                    }

                    layers[i].nodes.GetCollection()[j].value = value;

                    foreach(ulong connectionID in layers[i].nodes.GetCollection()[j].outgoingConnections)
                    {
                        connections.Get(connectionID).value = value;
                    }
                }
                catch(System.NullReferenceException e)
                {
                    break;
                }
            }
        }
    }

    public void fillFirstLayerData(double[] values)
    {
        for (int i = 0; i < values.Length; ++i)
        {
            layers[0].nodes.GetCollection()[i].value = values[i];
        }
    }

    public double[] getOutputNodeValues()
    {
        double[] values = new double[layers[layers.Count - 1].nodes.Length];

        for(int i = 0; i < values.Length; ++i)
        {
            values[i] = layers[layers.Count - 1].nodes.GetCollection()[i].value;
        }

        return values;
    }

    public static double Sigmoid(double value)
    {
        double k = System.Math.Exp(value);
        return k / (1.0 + k);
    }

    public void UpdateConnectionWeight(ulong id, float weight)
    {
        connections.Get(id).weight = weight;
        ColorCodeConnection(connections.Get(id).model, weight);
    }

    private void UpdateConnectionTransform(GameObject connection, Transform incNode, Transform outNode)
    {
        connection.transform.position = (incNode.position + outNode.position) / 2;

        connection.transform.LookAt(outNode);
        connection.transform.Rotate(new Vector3(90.0f, 0.0f, 0.0f));

        Vector3 scale = connection.transform.localScale;
        scale.y = Vector3.Distance(incNode.position, outNode.position) / 2;

        connection.transform.localScale = scale;
    }

    private void ColorCodeConnection(GameObject obj, double weight)
    {
        Material mat = obj.GetComponent<Renderer>().material;

        float clampedWeight = ((float)weight + 1) / 2;

        mat.color = new Color(clampedWeight, clampedWeight, clampedWeight);
    }
    
    public struct Node
    {
        public ulong id;
        public int layer;

        public GameObject model;

        public double value;
        
        public List<ulong> incomingConnections;
        public List<ulong> outgoingConnections;
    }

    public struct Connection
    {
        public ulong id;

        public GameObject model;

        public ulong incoming;
        public ulong outgoing;
        public double weight;
        public double value;
    }





    //Debug functions

    public double[] debug_fillFirstLayerWithRandomData()
    {
        double[] values = new double[layers[0].nodes.Length];

        for (int i = 0; i < values.Length; ++i)
        {
            values[i] = Random.Range(0.0f, 1.0f);
            layers[0].nodes.GetCollection()[i].value = values[i];
        }

        return values;
    }
}

