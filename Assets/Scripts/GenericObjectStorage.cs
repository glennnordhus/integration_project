﻿using UnityEngine;
using System.Collections;

public class GenericObjectStorage<T>
{
    protected T[] objects;
    protected Index[] indexes;

    protected struct Index
    {
        public ulong id;
        public uint index;
    }

    protected static ulong id = 0;
    protected uint length = 0;
    public uint Length { get => length;}

    public GenericObjectStorage()
    {
        objects = new T[2];
        indexes = new Index[2];
    }

    public GenericObjectStorage(int size)
    {
        int newSize = NextPowerOf2(size);
        objects = new T[newSize];
        indexes = new Index[newSize];
    }
    
    //TODO: Make resize work with unsigned int somehow, math needed
    public void Resize(int size)
    {
        int newSize = NextPowerOf2(size);     
        System.Array.Resize<T>(ref objects, newSize);
        System.Array.Resize<Index>(ref indexes, newSize);
    }

    public virtual ulong Add(T obj)
    {
        if (length == objects.GetUpperBound(0) + 1)
        {
            //Calculating the next power of 2 with a power of 2 gives the next number in the set
            Resize((int)length);
        }

        Index index = new Index
        {
            id = id++,
            index = length
        };

        indexes[length] = index;
        objects[length] = obj;

        length++;

        return index.id;
    }

    public virtual void Delete(ulong id)
    {
        uint objectIndex = uint.MaxValue;
        uint lastObjectIDIndex = uint.MaxValue;

        for (uint i = 0; i < length; ++i)
        {
            //Finds the Index of the object to delete and deletes it.
            if(indexes[i].id == id)
            {
                objectIndex = indexes[i].index;

                indexes[i] = indexes[length - 1];
                indexes[length - 1] = new Index();
            }

            //Finds the Index of the last object in the objects array.
            //Indexes have already been checked at this point and will not change from the object index deletion
            if(indexes[i].index == length - 1)
            {
                lastObjectIDIndex = i;
            }
        }

        Debug.Assert(objectIndex != uint.MaxValue, "Tried deleting something that doesn't exist, and we can't have that: ObjectIndex = " + objectIndex + " and lastObjectIDIndex = " + lastObjectIDIndex);

        if(objectIndex == length - 1)
        {
            objects[length - 1] = default;
        }
        else
        {
            objects[objectIndex] = objects[length - 1];
            objects[length - 1] = default;

            indexes[lastObjectIDIndex].index = objectIndex;
        }
       
        
        length--;
    }

    public ref T[] GetCollection()
    {
        return ref objects;
    }

    public ulong GetNextID()
    {
        return id;
    }

    public ref T Get(ulong id)
    {
        for (uint i = 0; i < length; ++i)
        {
            if (indexes[i].id == id)
            {
                return  ref objects[indexes[i].index];
            }
        }

        throw new System.Exception("ID does not exist");
    }

    public void Set(T obj, ulong id)
    {
        for(uint i = 0; i < length; ++i)
        {
            if(indexes[i].id == id)
            {
                objects[indexes[i].index] = obj;
            }
        }
    }

    protected int NextPowerOf2(int n)
    {
        n |= (n >> 16);
        n |= (n >> 8);
        n |= (n >> 4);
        n |= (n >> 2);
        n |= (n >> 1);
        ++n;
        return n;
    }
}