﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectStorage : GenericObjectStorage<GameObject>
{
    public override void Delete(ulong id)
    {
        GameObject.Destroy(Get(id));

        base.Delete(id);
    }
}
