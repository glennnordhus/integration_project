﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Neural_Network_Layer
{
    public readonly GenericObjectStorage<Neural_Network_Data.Node> nodes;
    public Activation_Function function;
    public static float lengthBetweenNodes = 2.0f;
    Generate_Network parent;
    GameObject layerObject;
    int layer;

    public Neural_Network_Layer(Layer_Type type, uint number_of_nodes, Vector3 offset, Generate_Network parent, int layer, uint nodes_per_row = 10)
    {
        nodes = new GenericObjectStorage<Neural_Network_Data.Node>();
        this.parent = parent;

        this.layer = layer;

        function = Activation_Function.LINEAR;


        //TODO: Seperate input and output layers into their own objects
        layerObject = new GameObject("Layer " + layer);
        layerObject.transform.parent = parent.transform;
        layerObject.transform.localPosition = offset;

        GenerateNodes(number_of_nodes, nodes_per_row);

        //switch(type)
        //{
        //    case Layer_Type.DENSE:
        //        GenerateDense(number_of_nodes, nodes_per_row);
        //        break;
        //
        //    default:
        //        throw new System.Exception("We have not implemented this yet");
        //}
    }

    public ulong GenerateNewNode(Vector3 position)
    {
        Neural_Network_Data.Node newNode = new Neural_Network_Data.Node
        {
            id = nodes.GetNextID(),

            model = GameObject.Instantiate(parent.Node_Prefab, layerObject.transform),

            incomingConnections = new List<ulong>(),
            outgoingConnections = new List<ulong>(),
            layer = layer,
        };

        newNode.model.transform.localPosition = position;

        newNode.model.GetComponent<NodeInteraction>().nodeID = newNode.id;
        newNode.model.GetComponent<NodeInteraction>().layer = newNode.layer;

        nodes.Add(newNode);

        return newNode.id;
    }


    private void GenerateNodes(uint number_of_nodes, uint nodes_per_row)
    {
        uint rows = (number_of_nodes / nodes_per_row);
        uint nodes_in_last_row = (number_of_nodes % nodes_per_row);
        Vector3 newPosition;
        for (int i = 0; i < rows; ++i)
        {
            for (int j = 0; j < nodes_per_row; ++j)
            {
                newPosition = new Vector3(j * lengthBetweenNodes + 0.1f, i * lengthBetweenNodes + 0.1f, 0.0f);

                GenerateNewNode(newPosition);
            }
        }

        for (int i = 0; i < nodes_in_last_row; ++i)
        {
            newPosition = new Vector3(i * lengthBetweenNodes + 0.1f, (rows) * lengthBetweenNodes + 0.1f, 0.0f);

            GenerateNewNode(newPosition);
        }
    }

    public void nameNodes(string[] names)
    {
        Debug.Assert(names.Length == nodes.Length, "Number of names sent in does not match the number of nodes");
        
        for(int i = 0; i <  nodes.Length; ++i)
        {
            UnityEngine.UI.Text text = nodes.GetCollection()[i].model.transform.GetChild(0).GetChild(0).GetComponent<UnityEngine.UI.Text>();
            text.text = names[i];
        }
    }

    public enum Activation_Function
    {
        SIGMOID,
        LINEAR
    }

    public enum Layer_Type
    {
        DENSE
    }
}
